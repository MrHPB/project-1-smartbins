from django.urls import path
from . import views

app_name = 'app'

urlpatterns = [

    #URLs for pages
    path('', views.index, name='index'),
    path('login/', views.login_view, name='login_view'), 
    path('logout/', views.logout_view, name='logout_view'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('bin/<int:id>/', views.bin_page, name='bin_page'),
    path('historical_bin_data/<int:id>/', views.historical_bin_data, name='historical_bin_data'),
    path('about/', views.about, name='about'),
    path('auth/', views.auth_view, name='auth'),
    path('register/', views.create_account, name='create_account'),
    path('bin_management/', views.bin_management, name='bin_management'),
    path('account/', views.account, name='account'),

    #URLs for device functionality
    path('insert_device_data/', views.insert_device_data, name='insert_device_data'),
    path('create_new_bin/', views.create_new_bin, name='create_new_bin'),
    path('get_time/', views.get_time, name='get_time'),   

    #URLs for Bin CRUD functionality
    path('create_bin/', views.create_bin, name='create_bin'),
    path('edit_bin/', views.edit_bin, name='edit_bin'),
    path('delete_bin/', views.delete_bin, name='delete_bin'),

    path('create_campus/', views.create_campus, name='create_campus'),
    path('edit_campus/', views.edit_campus, name='edit_campus'),
    path('delete_campus/', views.delete_campus , name='delete_campus'),

    path('create_bin_type/', views.create_bin_type, name='create_bin_type'),
    path('edit_bin_type/', views.edit_bin_type, name='edit_bin_type'),
    path('delete_bin_type/', views.delete_bin_type , name='delete_bin_type'),

    #URLs for User CRUD functionality
    path('user_management/', views.user_management, name='user_management'),
    path('user/<int:id>/', views.user_admin, name='user_admin'),
    path('delete_user/', views.delete_user, name='delete_user'), 

    #URLs for JSON/AJAX functionality
    path('get_bins/', views.get_bins, name='get_bins'),
    path('get_bin_logs/', views.get_bin_logs, name='get_bin_logs'),
    path('get_bin_logs_date/', views.get_bin_logs_date, name='get_bin_logs_date'),
    path('get_campuses/', views.get_campuses, name='get_campuses'),      
    
    #URLs for CSV functionality
    path('export_csv/', views.export_csv, name='export_csv'),
    path('report_csv/', views.report_csv, name='report_csv'),    

    #URLs for testing
    path('sample_data/', views.generate_sample_data, name='sample_data'),
    path('delete_all_data/', views.delete_all_data, name='delete_all_data'),
    path('bulk_sample_data/', views.generate_bulk_sample_data, name='bulk_sample_data'),
    path('testing/', views.testing),
    path('test_mail/', views.test_mail, name='test_mail'),

]