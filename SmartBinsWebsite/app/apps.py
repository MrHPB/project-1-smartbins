from django.apps import AppConfig
from SmartBinsAutomation import scheduler

#This subclass of AppConfig is used to configure the website
class SmartBinsConfig(AppConfig):
    
    name = 'app'
    
    #This function is called when the website is booted up
    def ready(self):

        #Start the smart bin scheduler 
        scheduler.start()