from django import forms
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth.models import User
from app.models import *

class RegistrationForm(UserCreationForm):
    
    first_name = forms.CharField(label='First Name:', widget=forms.TextInput(
        attrs={'class':'form-control my-1 mr-sm-2'
        }
    ))

    last_name = forms.CharField(label='Last Name:', widget=forms.TextInput(
        attrs={'class':'form-control my-1 mr-sm-2'
        }
    ))

    email = forms.EmailField(label='Email:', widget=forms.EmailInput(
        attrs={'class':'form-control my-1 mr-sm-2'
        }
    ))
   
    username = forms.CharField(label='Username:', widget=forms.TextInput(
        attrs={'class':'form-control my-1 mr-sm-2'
        }
    ))

    password1 = forms.CharField(label='Password:', widget=forms.PasswordInput(
        attrs={'class':'form-control my-1 mr-sm-2'
        }
    ))

    password2 = forms.CharField(label='Confrim Password:', widget=forms.PasswordInput(
        attrs={'class':'form-control my-1 mr-sm-2'
        }
    ))

    class Meta:

        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'    
        )

    def save(self, commit=True):

        user = super(RegistrationForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()
        
        return user


class PasswordChangeForm1(PasswordChangeForm):

    old_password = forms.CharField(label='Old Password:', widget=forms.PasswordInput(
        attrs={
            'class':'form-control mb-2 mr-sm-2'
        }
    ))

    new_password1 = forms.CharField(label='New Password:', widget=forms.PasswordInput(
        attrs={
            'class':'form-control mb-2 mr-sm-2'
        }
    ))

    new_password2 = forms.CharField(label='Confrim Password:', widget=forms.PasswordInput(
        attrs={
            'class':'form-control mb-2 mr-sm-2'
        }
    ))