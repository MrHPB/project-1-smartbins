from django.test import TestCase
from app.models import Bins
from app.models import BinTypes
import unittest

#class BinsModelTest(TestCase):
    #def setup(self):
        #Bins.objects.create(BinID="1", CampusID="4", CampusLocationID="4532")

    #def test_bins_creation(self):
        #test_bin = Bins.objects.get(BinID="1")
        #self.assertTrue(isinstance(test_bin, Bins))
        #self.assertEqual(test_bin.__unicode__(), test_bin.title)

#class BinTypeTest(TestCase):
    #def setUp(self):
        #BinTypes.objects.create(type_id="1", type_description="Recycle", total_volume="10", height="30")

class ModelTestCases(unittest.TestCase):

    def create_bin_type(self):
        
        return BinTypes.objects.create(type_id=1, type_description="Test Bin Type", total_volume=500, height=100)

    def create_campus(self):

        return Campuses.objects.create(campus_id=1, name="Test Campus")

    def create_campus_location(self):

        return CampusLocations.objects.create(location_id=1, campus_id=1, description="Test Campus Location")

    def create_bin(self):

        return Bins.objects.create(bin_id=1, campus_id=1, campus_location_id=1, type_id=1)

    def create_device_log(self):

        return Bins.objects.create(device_log_id=1, bin_id=1, distance=20, battery_remaining=75)

    def test_bin_type_creation(self):

        test_bin_type = self.create_bin_type()

        self.assertTrue(isinstance(test_bin_type, BinTypes))

        self.assertTrue(test_bin_type.get_id() == 1)
        self.assertTrue(test_bin_type.get_type_description() == "Test Bin Type")
        self.assertTrue(test_bin_type.get_total_volume() == 500)
        self.assertTrue(test_bin_type.get_height() == 100)
    
    def test_campus_creation(self):

        test_campus = self.create_campus()

        self.assertTrue(isinstance(test_campus, Campuses))

        self.assertTrue(test_campus.get_id() == 1)
        self.assertTrue(test_campus.get_name() == "Test Campus")

    def test_campus_location_creation(self):

        test_campus_location = self.create_campus_location()

        self.assertTrue(isinstance(test_campus_location, CampusLocations))

        self.assertTrue(test_campus_location.get_id() == 1)
        self.assertTrue(test_campus_location.get_campus_id().get_id() == 1)
        self.assertTrue(test_campus_location.get_description() == "Test Campus Location")
