from django.contrib import admin
from app.models import *

#Register the BinTypes, Campuses, Bins and DeviceLogs models
admin.site.register(BinTypes)
admin.site.register(Campuses)
admin.site.register(Bins)
admin.site.register(DeviceLogs)