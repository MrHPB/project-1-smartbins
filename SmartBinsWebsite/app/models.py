from django.db import models

class BinTypes(models.Model):

    #The fields for the BinTypes table
    type_id = models.AutoField(primary_key=True)
    type_description = models.CharField(max_length=50)
    total_volume = models.IntegerField()
    height = models.IntegerField()

    #Returns the ID of a bin type
    def get_id(self):

        return self.type_id

    #Returns the description of a bin type
    def get_type_description(self):

        return self.type_description

    #Returns the Total Volume of a bin type
    def get_total_volume(self):

        return self.total_volume

    #Returns the Height of a bin type
    def get_height(self):

        return self.height

class Campuses(models.Model):

    #The fields for the Campuses table
    campus_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    lat = models.CharField(max_length=50)
    lng = models.CharField(max_length=50)


    #Returns the ID of a campus
    def get_id(self):

        return self.campus_id

    #Returns the Name of a campus
    def get_name(self):

        return self.name

    #Returns the campus latitude
    def get_lat(self):

        return self.lat

    #Return the campus longitude
    def get_lng(self):

        return self.lng

class Bins(models.Model):

    #The fields for the Bins table
    bin_id = models.AutoField(primary_key=True)
    campus_id = models.ForeignKey(Campuses, on_delete=models.CASCADE)
    type_id = models.ForeignKey(BinTypes, on_delete=models.CASCADE)
    location_description = models.CharField(max_length=50)
    lat = models.CharField(max_length=50)
    lng = models.CharField(max_length=50)

    #Returns the id of a bin
    def get_id(self):

        return self.bin_id

    #Returns the id of the campus a bin is located at
    def get_campus_id(self):

        return self.campus_id.get_id()

    #Return the id of the bin type that a bin belongs to
    def get_type_id(self):

        return self.type_id

    #Returns all logs associated with a Bin in order from most recent to oldest
    def get_all_logs(self):

        #If the bin has been assigned an id
        if self.get_id():

            #Retrieve the bin's id
            bin_id = self.get_id()

            #Retrieve all logs associated with the bin in order from most recent to oldest
            all_logs = DeviceLogs.objects.filter(bin_id=bin_id).order_by('-log_timestamp')

            #Return the ordered list of bins
            return all_logs

    #Returns the most recent log for a bin, if it exists
    def get_most_recent_log(self):

        #If the bin has at least on device associated with it
        if self.get_all_logs().first():

            #Retrieve the most recent log
            most_recent_log = self.get_all_logs().first()

            #Return the most recent log
            return most_recent_log

    #Return the height of a bin
    def get_height(self):

        #Retrieve the bin's type
        bin_type = self.get_type_id()

        #Retrieve the height of the bin's type
        height = bin_type.get_height()

        #Return the bin's height
        return height

    #Return the total volume of a bin
    def get_total_volume(self):

        #Retrieve the bin's type
        bin_type = self.get_type_id()

        #Retrieve the total volume of the bin's type
        total_volume = bin_type.get_total_volume()

        #Return the bin's total volume
        return total_volume

    #Return the the description of a bin's type
    def get_type_description(self):

        #Retrieve the bin's type
        bin_type = self.get_type_id()

        #Retrieve the description of the bin's type
        type_description = bin_type.get_type_description()

        #Return the description of the bin's type
        return type_description

    #Return the name of the campus the bin is located in
    def get_campus_name(self):

        #Retrieve the bin's campus
        campus = self.get_campus()

        #Retrieve the name of the campus the bin is located at
        campus_name = campus.get_name()

        #Return the name of the campus the bin is located at
        return campus_name

    #Returns the description of the bin's location
    def get_campus_location_description(self):

        return self.location_description

    #Returns Campus Object
    def get_campus(self):
        
        return self.campus_id

    #Returns the bin's latitude
    def get_lat(self):

        return self.lat

    #Return the bin's longitude
    def get_lng(self):

        return self.lng

    def get_current_fill(self):
        if(self.get_most_recent_log()):
            return self.get_most_recent_log().get_fill_level()
        else:
            return None

    def get_current_battery(self):
        return self.get_most_recent_log().get_battery_remaining()


class DeviceLogs(models.Model):

    #The fields for the DeviceLogs table
    device_log_id = models.AutoField(primary_key=True)
    log_timestamp = models.DateTimeField(auto_now_add=True)
    bin_id = models.ForeignKey(Bins, on_delete=models.CASCADE)
    distance = models.IntegerField()
    battery_remaining = models.IntegerField()
    
    #Set up the table so that the log timestamp and bin id fields must be unique
    # class Meta:

    #     constraints = [
    #         models.UniqueConstraint(fields=['log_timestamp', 'bin_id'], name='deviceLog'),
    #     ]

    #Returns the id of a device log
    def get_id(self):

        return self.device_log_id

    #Returns the timestamp of a device log
    def get_log_timestamp(self):

        return self.log_timestamp

    #Returns the id of the bin associated with a device log
    def get_bin_id(self):

        return self.bin_id

    #Returns the distance measured in a device log
    def get_distance(self):

        return self.distance

    #Returns the battery percentage remaining in a device log
    def get_battery_remaining(self):

        return round(self.battery_remaining, 2) 

    #Returns the fill level of a Bin associated with a DeviceLog object
    def get_fill_level(self):

        #Retrieve the bin the log is reporting on
        bin_instance = self.get_bin_id()
        
        #Retrieve the height of the bin and the distance of the waste from the sensor
        height = bin_instance.get_height()
        distance = self.get_distance()

        #If there is a measurment error where the distance is greater than the height of the bin the distance is capped at the height of the bin type
        if distance > height:
            distance = height

        #Get the fill level of the bin
        fill_level = distance / height
        fill_level = ((1 - fill_level) * 100)

        #Return the fill level of the bin rounded to 2 decimal places
        return round(fill_level, 2)