from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, JsonResponse, HttpResponseNotFound

from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from app.models import *
from app.forms import RegistrationForm, PasswordChangeForm1

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required

from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.shortcuts import render, redirect

import csv
from datetime import datetime
from django.core.mail import send_mail, mail_admins

#This import is only used for testing purposes
import random

##################################################
# views for pages
##################################################

#Index Page View
def index(request):
    if request.user.is_authenticated:
        return dashboard(request)
    else:
        return login_view(request)

def create_account(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            username = form.data['username']
            password = form.data['password1']
            first_name = form.data['first_name']
            last_name = form.data['last_name']
            email = form.data['email']

            form.save()
            #Attempt to authenticate the user
            user = authenticate(username=username, password=password)

            #If account creation is successfull, login the user and direct them to their Account page
            if user is not None:
                user.first_name = first_name
                user.last_name = last_name
                user.email = email
                
                user.save()

                login(request, user)
                return HttpResponseRedirect('/app/account')
    
    else:
        form = RegistrationForm()
    return render(request, 'app/createaccount.html', {'form': form })


#Account View
@login_required(login_url = '/app/login/')
def account(request):
    if request.POST.get("change_password"):
        form = PasswordChangeForm1(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return render(request, 'app/account.html', {
        'form': form
        })
        else:
            return render(request, 'app/account.html', {
            'form': form
            })
    elif request.POST.get("save"): 
        user = request.user
        user.first_name = request.POST.get("first_name")
        user.last_name = request.POST.get("last_name")
        user.email = request.POST.get("email")
        user.save()

        messages.success(request, 'Your details were successfully updated!')
        form = PasswordChangeForm1(request.user) 
        return render(request, 'app/account.html', {
        'form': form
        })
    else:
        form = PasswordChangeForm1(request.user) 
        return render(request, 'app/account.html', {
        'form': form
    })
    

#Dashboard Page View
@login_required(login_url = '/app/login/')
def dashboard(request):

    template = loader.get_template('app/dashboard.html')

    #Retrieve all bins
    all_bins = Bins.objects.all()

    recent = {}
    for bins in all_bins:
        recent.update({bins : bins.get_most_recent_log()})
    
    all_campuses = Campuses.objects.all()
    all_bin_types = BinTypes.objects.all()

    context = {
        'recent' : recent,
        'all_bins' : all_bins,
        'all_campuses' : all_campuses,
        'all_bin_types' : all_bin_types,
    }

    return HttpResponse(template.render(context, request))

#Individual Bin Page View
@csrf_exempt
@login_required(login_url = '/app/login/')
def bin_page(request, id):

    bin_id = id
    
    
    if Bins.objects.filter(bin_id=bin_id).exists():
        bin_instance = Bins.objects.get(bin_id=bin_id)
        #Retrieve the template for the individual bin page
        template = loader.get_template('app/binpage.html')

        #Retrieve the bin specified by the id in the request and its most recent log
        
        most_recent_log = bin_instance.get_most_recent_log()

        #Retrieve all campuses and bin types
        all_campuses = Campuses.objects.all()
        all_bin_types = BinTypes.objects.all()
        
        #Pass the bin, its most recent log and all campuses and bin types to the page for the bin
        context = {
            'bin': bin_instance,
            'most_recent_log': most_recent_log,
            'all_campuses': all_campuses,
            'all_bin_types': all_bin_types,
        }
        return HttpResponse(template.render(context, request))
    else:
        template = loader.get_template('app/404.html')
        return HttpResponseNotFound(template.render({'a':"Not Found"}, request))
    
    

#List of all Logs for a Bin View
@csrf_exempt
@login_required(login_url = '/app/login/')
def historical_bin_data(request, id):

    template = loader.get_template('app/historicalbindata.html')

    #Retrieve the bin specified by the id in the request and all of its logs
    bin_id = id
    if Bins.objects.filter(bin_id=bin_id).exists():
        bin_instance = Bins.objects.get(bin_id=bin_id)
        all_logs = bin_instance.get_all_logs()

        #Pass the bin and all of its logs to the historical bin data page
        context = {
            'bin': bin_instance,
            'all_logs': all_logs,
        }

        return HttpResponse(template.render(context, request))
    else:
        template = loader.get_template('app/404.html')
        return HttpResponseNotFound(template.render({'a':"Not Found"}, request))
  

#About Page View
def about(request):
    
    return render(request, 'app/about.html')

##################################################
# views for device functionality
##################################################

#This view will be used by devices when they have data they want send to the server and have stored in the database
@csrf_exempt
def insert_device_data(request):

    #If the bin id, distance and battery variables exist in the POST request sent from the device
    if 'bin_id' in request.POST and 'distance' in request.POST and 'battery' in request.POST:

        #Retrieve the bin id, distance and battery remaining variables from the POST request
        bin_id = request.POST.get('bin_id', '')
        distance = float(request.POST.get('distance', ''))
        battery = float(request.POST.get('battery', ''))

        #If a bin with the id specified in the recieved POST request exists
        if(Bins.objects.filter(bin_id=bin_id).exists()):

            #Retrieve the bin that has the id retrieved from POST
            bin_instance = Bins.objects.get(bin_id=bin_id)
            bin_height = bin_instance.get_height()

            #If an error occurs with the distance recieved and it is less than 0, set the distance to 0
            if distance < 0:
                distance = 0
            #Else if an occurs with the distance recieved and it is greater than the bin's height, set the distance to the bin's height
            elif distance > bin_height:
                distance = bin_height

            #If an error occurs with the battery percentage recived and it is less than 0, set the battery percentage to 0
            if battery < 0:
                battery = 0
            #Else if an errors with the battery percentage recieved and it is greater than 100, set the battery percentage to 100
            elif battery > 100:
                battery = 100

            #Create a new entry in the DeviceLogs table
            device_log_entry = DeviceLogs(
                bin_id=bin_instance,
                distance=distance,
                battery_remaining=battery
            )

            #Insert the new entry into the DeviceLogs table
            device_log_entry.save()

            #Send a HTTP response to the device that signifies the data sent was successfully recieved
            return HttpResponse("inserted_data=true&end_of_data=true")
    
        #If a bin with the specified id does not exist
        else:
            
            #Send a HTTP response to the device that signifies an error occured
            return HttpResponseBadRequest("Device does not exist")

    #If the bin_id, distance and battery variables do not exist in POST
    else:

        #Send a HTTP response to the device that signifies an error occured
        return HttpResponseBadRequest()

#This view is used by a device when it is started for the first time and needs to be assigned an id so it can identify itself to the server in the future. This view will also create a bin to represent that device in the database
@csrf_exempt
def create_new_bin(request):

    #Retrieve the number of bins stored in the database
    num_bins = Bins.objects.count()

    #If there are no bins in the database
    if num_bins == 0:

        default_latitude = "-37.626415"
        default_longitude = "143.891031"

        #Create a default campus for future bins to use
        default_campus = Campuses(campus_id=1, name="Mt Helen", lat=default_latitude, lng=default_longitude)
        default_campus.save()

        #Create a default bin type for future bins to use
        default_bin_type = BinTypes(type_id=1, type_description="General", total_volume=100, height=100)
        default_bin_type.save()

        #Insert the first bin into the database
        new_bin = Bins(campus_id=default_campus, location_description="DEFAULT", type_id=default_bin_type, lat=default_latitude, lng=default_longitude)
        new_bin.save()

        #Return a response to the device indicating that the id to be stored in the bin is 1 and that the request was successfull
        return HttpResponse("bin_id=1&end_of_data=true")

    #If there is at least one bin in the database
    else:
        
        #Retrieve the default campus and bin type
        default_campus = Campuses.objects.get(campus_id=1)
        default_bin_type = BinTypes.objects.get(type_id=1)
        
        #Insert a new bin into the database
        new_bin = Bins(campus_id=default_campus, location_description="DEFAULT", type_id=default_bin_type, lat=0, lng=0)
        new_bin.save()

        new_bin_id = new_bin.get_id()
        
        #Return a response to the device indicating the id to be stored in the bin and that the request was successfull
        return HttpResponse("bin_id=" + str(new_bin_id) + "&end_of_data=true")

#This view is used by the bin whenever it turns on so that it can determine the current time and set up its scheduling accordingly
@csrf_exempt
def get_time(request):

    #Retrieve the current date and time
    current_datetime = datetime.now()

    #Retrieve the current minute and second
    current_minute = current_datetime.minute
    current_second = current_datetime.second

    #Return a response to the device indicating that the current minute and second and that the request was successfull
    return HttpResponse("current_second=" + str(current_second) + "&current_minute=" + str(current_minute) + "&end_of_data=true")

##################################################
# views for bin CRUD functionality
##################################################

@staff_member_required
def bin_management(request):

    template = loader.get_template('app/binmanagement.html')

    all_bin_types = BinTypes.objects.all()
    all_campuses = Campuses.objects.all()
    all_bins = Bins.objects.all

    context = {
        'all_bin_types': all_bin_types,
        'all_campuses': all_campuses,
        'all_bins': all_bins,
    }

    return HttpResponse(template.render(context, request))

@staff_member_required
def edit_bin(request):

    if request.method == 'POST': 
 
        bin_id = str(request.POST.get('id', '')) 
 
        bin_instance = Bins.objects.get(bin_id = bin_id) 
 
        campus = str(request.POST.get('campus', ''))
        bin_type = str(request.POST.get('bin_type', ''))
        campus_location = str(request.POST.get('campus_location', ''))
        lat = str(request.POST.get('lat', ''))
        lng = str(request.POST.get('lng', ''))

 
        bin_instance.campus_id = Campuses.objects.get(campus_id = campus) 
        bin_instance.type_id = BinTypes.objects.get(type_id =bin_type) 
        bin_instance.location_description = campus_location 
        bin_instance.lat = lat
        bin_instance.lng = lng
        
 
        bin_instance.save() 

    url = '/app/bin/' + bin_id

    return HttpResponseRedirect(url)


@staff_member_required
def create_campus(request):
    #If the request is a POST request
    if request.method == 'POST':

        campus = str(request.POST.get('campus', ''))
        lat = str(request.POST.get('lat', ''))
        lng = str(request.POST.get('lng', ''))
        
        new_campus = Campuses(name=campus, lat=lat, lng=lng)
        new_campus.save()

        return bin_management(request)


#Create Bin View (Testing version)
@staff_member_required
def create_bin(request):
   
    #If the request is a POST request
    if request.method == 'POST':

        #Retrieve the campus id, campus location, bin type, latitude and longitude from the request
        campus_id = Campuses.objects.get(campus_id=str(request.POST.get('campus', '')))
        campus_location = str(request.POST.get('campus_location', ''))
        type_id = BinTypes.objects.get(type_id=str(request.POST.get('bin_type', '')))
        lat = str(request.POST.get('lat', ''))
        lng = str(request.POST.get('lng', ''))

        #Add the new bin to the database
        new_bin = Bins(campus_id=campus_id, location_description=campus_location, type_id=type_id, lat=lat, lng=lng)
        new_bin.save()

        return bin_management(request)

@staff_member_required
def delete_bin(request):
    
    #If the request is a POST request
    if request.method == 'POST':

        #Retrieve the ids of the bins to be deleted from the POST request
        bins = request.POST.getlist('id')

        for bin_id in bins:
            requested_bin = Bins.objects.get(bin_id=bin_id)
            requested_bin.delete()

        return bin_management(request)

@staff_member_required
def create_bin_type(request):
   
    #If the request is a POST request
    if request.method == 'POST':
        type_description = str(request.POST.get('type_description', ''))
        total_volume = str(request.POST.get('total_volume', ''))
        height = str(request.POST.get('height', ''))
        
        new_bin_type = BinTypes(type_description=type_description, total_volume=total_volume, height=height)
        new_bin_type.save()

        return bin_management(request)

@staff_member_required
def delete_bin_type(request):

    #If the request is a POST request
    if request.method == 'POST':

        bin_types = request.POST.getlist('id')

        for bin_type_id in bin_types:
            requested_bin_type_id = BinTypes.objects.get(type_id=bin_type_id)
            requested_bin_type_id.delete()

        return bin_management(request)

@staff_member_required
def delete_campus(request):

    #If the request is a POST request
    if request.method == 'POST':

        campuses = request.POST.getlist('id')

        for campus_id in campuses:
            requested_campus = Campuses.objects.get(campus_id=campus_id)
            requested_campus.delete()

        return bin_management(request)

@staff_member_required
def edit_campus(request):

    #If the request is a POST request
    if request.method == 'POST':

        campus = request.POST.get('campus')
        new_name = request.POST.get('campus_name')
        new_lat = request.POST.get('lat')
        new_lng = request.POST.get('lng')
    
        campus_instance = Campuses.objects.get(campus_id=campus)
        campus_instance.name = new_name
        campus_instance.lat = new_lat
        campus_instance.lng = new_lng

        campus_instance.save()

        return bin_management(request)

@staff_member_required
def edit_bin_type(request):

    #If the request is a POST request
    if request.method == 'POST':

        bin_type_id = request.POST.get('bin_type_id')
        new_type = request.POST.get('new_type_description')
        new_height = request.POST.get('new_height')
        new_volume = request.POST.get('new_volume')
    
        type_instance = BinTypes.objects.get(type_id=bin_type_id)
        type_instance.type_description = new_type
        type_instance.total_volume = new_volume
        type_instance.height = new_height

        type_instance.save()

        return bin_management(request)


##################################################
# views for user CRUD functionality
##################################################

@staff_member_required
def user_management(request):

    template = loader.get_template('app/usermanagement.html')

    all_users = User.objects.all()

    context = {
        'all_users': all_users,
    }

    return HttpResponse(template.render(context, request))

@staff_member_required
def user_admin(request, id):
    template = loader.get_template('app/useradmin.html')
    
    if request.POST.get("save"): 
        user = User.objects.get(id=request.POST.get("id"))
        user.first_name = request.POST.get("first_name")
        user.last_name = request.POST.get("last_name")
        user.email = request.POST.get("email")

        if request.POST.get("staff"):
            user.is_staff = True
        else:
            user.is_staff = False

        if request.POST.get("active"):
            user.is_active = True
        else:
            user.is_active = False

        user.save()

        messages.success(request, 'Your details were successfully updated!', extra_tags='alert-success')

        context = {
            'user_instance': user,
        }
    elif request.POST.get("change_password"):
        user = User.objects.get(id=request.POST.get("id"))
        password1 = request.POST.get("new_password1")
        password2 = request.POST.get("new_password2")
        if(password1 != "" and password1 == password2):
            user.set_password(password1)
            user.save()
            messages.success(request, "Password updated successfully", extra_tags='alert-success') 
        else:
            messages.error(request, "Passwords do not match", extra_tags='alert-danger')
        context = {
            'user_instance': user,
        }
        
    else: 
        user_id = id
        user_instance = User.objects.get(id=user_id)

        context = {
            'user_instance': user_instance,
        }
    return HttpResponse(template.render(context, request))


@staff_member_required
def delete_user(request):

    user_id = request.POST.get('id', '')
    user_instance = User.objects.get(id=user_id)

    if user_instance is not None: 
        user_instance.delete()

    return user_management(request)

##################################################
# views for logging in/out functionality
##################################################

#Authentication View
def auth_view(request):

    #Retrieve the username and password the user entered
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')

    #Attempt to authenticate the user
    user = authenticate(username=username, password=password)

    #If the user can be authenticated
    if user is not None:

        #Login the user and redirect them to the Dashboard page
        login(request, user)
        return dashboard(request)

    #If the user cannot be authenticated
    else:

        #Redirect the user to the Login page
        messages.error(request, 'Incorrect Username or Password!')
        return login_view(request)

#Login Page View
def login_view(request):

    return render(request, 'app/login.html')

#Logout View
def logout_view(request):

    logout(request)
    return login_view(request)

##################################################
# views for report generation functionality
##################################################

def export_csv(request):
    currenttime = datetime.now().strftime('%Y%m%d')
    filename = "BinReport" + currenttime + ".csv"  
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename= "{}"'.format(filename)

    all_logs = DeviceLogs.objects.all()
    
    writer = csv.writer(response)
    writer.writerow(['Log ID', 'Bin ID', 'Timestamp', 'Battery %', 'Distance', 'Fill %'])
    for logs in all_logs:
        row = logs.get_id(), logs.get_bin_id().bin_id, logs.get_log_timestamp().strftime("%d-%m-%Y %H:%M:%S"), logs.get_battery_remaining(), logs.get_distance(), logs.get_fill_level()
        writer.writerow(row)

    return response

def report_csv(request):
    currenttime = datetime.now().strftime('%Y%m%d')
    filename = "BinReport" + currenttime + ".csv"  
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename= "{}"'.format(filename)

    all_logs = []

    bins = request.POST.getlist('bin')

    campus = request.POST.get('campus')

    bin_type = request.POST.get('type')  

    if request.POST.get('to_date'):

        to_date = str(request.POST.get('to_date'))+" 23:59:59"
        from_date = str(request.POST.get('from_date'))+" 00:00:00"
        
        for bin_id in bins:
            requested_bin = Bins.objects.get(bin_id=bin_id)
            bin_logs = requested_bin.get_all_logs().filter(log_timestamp__range=(from_date, to_date))
            for l in bin_logs:
                all_logs.append(l)

    else:
        for bin_id in bins:
            requested_bin = Bins.objects.get(bin_id=bin_id)
            bin_logs = requested_bin.get_all_logs()
            for l in bin_logs:
                all_logs.append(l)

    if(BinTypes.objects.filter(type_id=int(bin_type)).exists()):
        bin_type_object = BinTypes.objects.get(type_id=int(bin_type))
        bin_string = bin_type_object.get_type_description() + " " + str(bin_type_object.get_height()) + "cm" +  " " + str(bin_type_object.get_total_volume()) + "L"
    else:
        bin_string = "All"        

    writer = csv.writer(response)
    writer.writerow(['Campus:', campus, 'Bin Type:', bin_string])
    writer.writerow(['Log ID', 'Bin ID', 'Timestamp', 'Battery %', 'Distance', 'Fill %'])
    for logs in all_logs:
        row = logs.get_id(), logs.get_bin_id().bin_id, logs.get_log_timestamp().strftime("%d-%m-%Y %H:%M:%S"), logs.get_battery_remaining(), logs.get_distance(), logs.get_fill_level()
        writer.writerow(row)

    return response

##################################################
# views for AJAX functionality
##################################################

def get_bins(request): 
 
    data = {} 
    bins_list = [] 
     
    all_bins = Bins.objects.all() 
 
    for bins in all_bins: 
        log = bins.get_most_recent_log()
        if log is None:
            bins_list.append({ 
                'id':bins.get_id(), 
                'campus_id':bins.get_campus_id(), 
                'campus_description':bins.get_campus_name(), 
                'type_id':bins.get_type_id().type_id, 
                'type_description':bins.get_type_description(), 
                'height':bins.get_height(), 
                'volume':bins.get_total_volume(), 
                'location_description':bins.get_campus_location_description(), 
                'lat':bins.get_lat(),
                'lng':bins.get_lng(),
                'recent_log_timestamp':0,
                'current_fill':0,
                'current_battery':0, 
            }) 
        else:
            bins_list.append({ 
                'id':bins.get_id(), 
                'campus_id':bins.get_campus_id(), 
                'campus_description':bins.get_campus_name(), 
                'type_id':bins.get_type_id().type_id, 
                'type_description':bins.get_type_description(), 
                'height':bins.get_height(), 
                'volume':bins.get_total_volume(), 
                'location_description':bins.get_campus_location_description(), 
                'lat':bins.get_lat(),
                'lng':bins.get_lng(),
                'recent_log_timestamp':log.get_log_timestamp().strftime("%d-%m-%Y %H:%M:%S"),
                'current_fill':log.get_fill_level(),
                'current_battery':log.get_battery_remaining(), 
            }) 
     
    data["all_bins"] = bins_list 
    return JsonResponse(data)

def get_bin_logs(request):
    
    bin_instance = Bins.objects.get(bin_id = request.GET.get('id', ''))
    
    if request.GET.get('value', ''):

        value = int(request.GET.get('value', ''))
        all_logs = bin_instance.get_all_logs()[:value]
    else:
        all_logs = bin_instance.get_all_logs()    
    
    data = {} 
    bin_request = [] 
    all_logs = reversed(all_logs)
    for logs in all_logs: 
        bin_request.append({ 
            'log_id':logs.get_id(), 
            'timestamp':logs.get_log_timestamp().strftime('%d-%m-%Y %H:%M:%S'), 
            'battery_percentage':logs.get_battery_remaining(), 
            'distance':logs.get_distance(), 
            'fill_percentage':logs.get_fill_level(), 
        }) 
     
    data["all_logs"] = bin_request 
    return JsonResponse(data)

def get_bin_logs_date(request):
    
    bin_instance = Bins.objects.get(bin_id = request.GET.get('id', ''))
    to_date = str(request.GET.get('to_date', ''))
    from_date = str(request.GET.get('from_date', ''))
    

    all_logs = bin_instance.get_all_logs().filter(log_timestamp__range=(from_date, to_date))

    data = {} 
    bin_request = [] 
    all_logs = reversed(all_logs)
    for logs in all_logs:
        
        bin_request.append({ 
            'log_id':logs.get_id(), 
            'timestamp':logs.get_log_timestamp().strftime('%d-%m-%Y %H:%M:%S'), 
            'battery_percentage':logs.get_battery_remaining(), 
            'distance':logs.get_distance(), 
            'fill_percentage':logs.get_fill_level(), 
        }) 
     
    data["all_logs"] = bin_request 
    return JsonResponse(data)


def get_campuses(request): 
 
    data = {} 
    campus_list = [] 
     
    all_campuses = Campuses.objects.all() 
 
    for campus in all_campuses: 
        campus_list.append({ 
            'id':campus.get_id(),  
            'campus_name':campus.get_name(), 
            'lat':campus.get_lat(),
            'lng':campus.get_lng(),
        }) 
     
    data["all_campuses"] = campus_list 
    return JsonResponse(data)

##################################################
# views for testing
##################################################

def testing(request):

    # get the template for the testing page
    template = loader.get_template('app/testing.html')

    # retrieve all bins from the database
    all_bins = Bins.objects.all

    #Pass all the bins to the testing page
    context = {
        'all_bins': all_bins
    }

    return HttpResponse(template.render(context, request))


def generate_sample_data(request):

    # create a new entry for the Bins, DeviceLogs, BinTypes, Campus

    bin_type_1 = BinTypes(
        type_id=1, type_description='Recycle', total_volume=50, height=300)
    bin_type_2 = BinTypes(
        type_id=2, type_description='General', total_volume=60, height=400)

    campus_1 = Campuses(campus_id=1, name='Mt Helen')
    campus_2 = Campuses(campus_id=2, name='Grampians')

    bin_1 = Bins(bin_id=1, campus_id=campus_1,
                    location_description="T Building Entrance", lat="-37.62686560987681", lng="143.89174895215513", type_id=bin_type_1)
    bin_2 = Bins(bin_id=2, campus_id=campus_1,
                    location_description="Quad", lat="-37.6262877929094", lng="143.89237122464658", type_id=bin_type_1)
    bin_3 = Bins(bin_id=3, campus_id=campus_1,
                    location_description="Quad", lat="-37.62652571808704", lng="143.89233367372037", type_id=bin_type_2)
    bin_4 = Bins(bin_id=4, campus_id=campus_1,
                    location_description="Quad", lat="-37.626483231504025", lng="143.8918696515608", type_id=bin_type_2)
    bin_5 = Bins(bin_id=5, campus_id=campus_1,
                    location_description="Quad", lat="-37.62638976093593", lng="143.8921217792082", type_id=bin_type_1)
    bin_6 = Bins(bin_id=6, campus_id=campus_1,
                    location_description="Admin building", lat="-37.626470485524386", lng="143.8914029471922", type_id=bin_type_2)
    
    device_log_1 = DeviceLogs(
        bin_id=bin_1, distance=20, battery_remaining=70)
    device_log_2 = DeviceLogs(
        bin_id=bin_2, distance=200, battery_remaining=71)
    device_log_3 = DeviceLogs(
        bin_id=bin_3, distance=30, battery_remaining=72)
    device_log_4 = DeviceLogs(
        bin_id=bin_4, distance=50, battery_remaining=79)
    device_log_5 = DeviceLogs(
        bin_id=bin_5, distance=250, battery_remaining=77)
    device_log_6 = DeviceLogs(
        bin_id=bin_6, distance=1, battery_remaining=76)

    # insert all the new entries into the database
    bin_type_1.save()
    bin_type_2.save()
    
    campus_1.save()
    campus_2.save()


    bin_1.save()
    bin_2.save()
    bin_3.save()
    bin_4.save()
    bin_5.save()
    bin_6.save()

    device_log_1.save()
    device_log_2.save()
    device_log_3.save()
    device_log_4.save()
    device_log_5.save()
    device_log_6.save()

    return dashboard(request)


def generate_bulk_sample_data(request):

    # create a new entry for the Bins, DeviceLogs, BinTypes, Campus, creates far more data than generate_sample_data method 

    bin_type_1 = BinTypes(
        type_id=1, type_description='Recycle', total_volume=50, height=150)
    bin_type_2 = BinTypes(
        type_id=2, type_description='General', total_volume=60, height=160)

    

    test_campuses = [Campuses(campus_id=1, name='Mt Helen', lat='-37.626565', lng='143.892474'), Campuses(campus_id=2, name='Gippsland', lat='-38.312340', lng='146.429395'), Campuses(campus_id=3, name='SMB', lat='-37.565712', lng='143.857229')]



    test_bins = set()
    binpairstogenerate = 50

    for i in range(1, binpairstogenerate+1):

        campus = test_campuses[random.randint(0,len(test_campuses)-1)]
        campus_id = campus.get_id()
        thelocation = ran_location()

        test_bins.add(Bins(bin_id=(2*i-1), campus_id=campus,
                    location_description=thelocation, lat=ran_lat(campus_id), lng=ran_lng(campus_id), type_id=bin_type_1))
        test_bins.add(Bins(bin_id=(2*i), campus_id=campus,
                    location_description=thelocation, lat=ran_lat(campus_id), lng=ran_lng(campus_id), type_id=bin_type_2))

    
    

    # insert all the new entries into the database
    bin_type_1.save()
    bin_type_2.save()

    for thecampus in test_campuses:

        thecampus.save()
    

    for thebin in test_bins:
        print(thebin)
        thebin.save()
        for i in range(0, 25):
            thelog = DeviceLogs(
                bin_id=thebin, distance=ran_distance(thebin), battery_remaining=ran_battery())
            thelog.save()


  
    return dashboard(request)

#random junk data to populate test bins

def ran_battery():

    return random.randint(0,100)

def ran_distance(thebin):

    return random.randint(0,thebin.get_height())

def ran_location():

    location = ['a', 'b', 'c', 'd']

    return location[random.randint(0,len(location)-1)]

def ran_lat(campus):
    if campus == 1:
        lat = random.uniform(-37.628801, -37.623308)
    elif campus == 2:
        lat = random.uniform(-38.308919, -38.314946)
    else:
        lat = random.uniform(-37.564690, -37.566952)
    return str(lat)

def ran_lng(campus):
    if campus == 1:
        lng = random.uniform(143.885855, 143.896793)
    elif campus == 2:
        lng = random.uniform(146.425668, 146.435492)
    else:
        lng = random.uniform(143.855844, 143.858342)
    
    return str(lng)

def delete_all_data(request):
    Bins.objects.all().delete()
    Campuses.objects.all().delete()
    BinTypes.objects.all().delete()

    return bin_management(request)

def test_mail(request):
    all_bins = Bins.objects.all()
    all_campuses = Campuses.objects.all()
    subject = 'Smart Bins Alerts - Bins That require Action!'
    message = 'Message Body Here'
    sender = 'Smart Bins Alerts <noreply.smartbins@gmail.com>'

    all_users = User.objects.all()
    recipients = []

    for users in all_users:
        if users.is_staff:
            recipients.append(users.email)


    html = loader.render_to_string('app/email_template.html',{
        'all_bins' : all_bins,
        'all_campuses' : all_campuses,
    })


    send_mail(subject, message, sender, recipients, fail_silently=False, html_message = html)

    return bin_management(request)