from app.models import *
from datetime import datetime
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.template import loader

def check_logs_uploaded_successfully():

    #Retrieve all admins from the database and create an empty list for storing their emails
    all_admins = User.objects.filter(is_superuser=1)
    admin_email_addresses = []

    #Iterate over all admins and store their email addresses in the previously created list
    for admin in all_admins:
        admin_email_addresses.append(admin.email)

    #Retrieve all bins from the database and create an empty list for storing bins that have failed to send data on time
    all_bins = Bins.objects.all()
    failed_bins = []

    #Retrieve all campuses from the database
    all_campuses = Campuses.objects.all()

    #Retrieve the current time
    current_time = datetime.now()
    current_minute = current_time.minute

    #Take 5 minutes off the current time to determine the timeframe in which all the devices should have submitted their data
    log_upload_timeframe = current_time
    log_upload_timeframe = log_upload_timeframe.replace(minute=current_minute-5)

    #Iterate through all bins
    for current_bin in all_bins:

        #Retrieve the timestamp of the latest log associated with the current bin
        latest_log_timestamp = current_bin.get_most_recent_log().get_log_timestamp()

        #If the latest log for the current bin was not submitted within the acceptable timeframe, add the bin to the list of failed bins
        if latest_log_timestamp < log_upload_timeframe:
            failed_bins.append(current_bin)

    #Determine the number of bins that failed to upload their data on time
    num_failed_bins = len(failed_bins)

    #If at least one bin failed to upload its data on time
    if num_failed_bins > 0:

        #If only one bin failed to upload its data on time, make the subject and message for the email to be sent indicate which bin failed
        if num_failed_bins == 1:
            subject = 'A Smart Bin device failed to send data'
            message = str(failed_bins[0].get_id()) + " has failed to send data on time."

        #If multiple bins failed to upload data on time, make the subject and message for the email to be sent indicate which bins failed
        else:
            subject = 'Multiple Smart Bin devices failed to send data'
            message = "Devices that failed to send data on time are bins:\n\n"
            for failed_bin in failed_bins:
                message += "Bin " + str(failed_bin.get_id()) + "\n"

        #Send the email indicating which device(s) failed to send data on time to admins
        send_mail(
            subject,
            message,
            'Smart Bins Alerts <noreply.smartbins@gmail.com>',
            admin_email_addresses,
            fail_silently=False,
        )


def daily_bin_report():

    all_bins = Bins.objects.all()
    all_campuses = Campuses.objects.all()

    subject = 'Smart Bins Alerts - Bins That require Action!'
    message = 'Message Body Here'
    sender = 'Smart Bins Alerts <noreply.smartbins@gmail.com>'

    all_admins = User.objects.filter(is_superuser=1)
    admin_email_addresses = []

    for admin in all_admins:

        admin_email_addresses.append(admin.email)


    html = loader.render_to_string('app/email_template.html',{
        'all_bins' : all_bins,
        'all_campuses' : all_campuses,
    })


    send_mail(
        subject,
        message,
        sender,
        admin_email_addresses,
        fail_silently = False,
        html_message = html
    )
