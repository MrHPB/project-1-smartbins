from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime

def start():

    from SmartBinsAutomation import jobs

    #Create a new background scheduler that can be used to execute specified jobs
    scheduler = BackgroundScheduler()

    #A list of all hours in the day in 24 hour format
    hours = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]

    #Make sure that the job for checking device logs will be run every single hour (+ 5 minutes) of the day. The 5 minutes is added to account for issues such as slow wi-fi for instance
    #for hour in hours:
        # scheduler.add_job(jobs.check_logs_uploaded_successfully, 'cron', hour=hour, minute=5)

    #Schedule the job for creating a daily report on all registered bins to run every day at 9:00am
    scheduler.add_job(jobs.daily_bin_report, 'cron', hour=9, minute=0)

    #Start the scheduler
    scheduler.start()