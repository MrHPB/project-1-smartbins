#include <EEPROM.h>

void setup() 
{

  Serial.begin(9600);

  //Wipe the device
  Serial.println("WIPING DEVICE");
  wipe();
  Serial.println("FINISHED WIPING DEVICE");

}

void loop() 
{
  //Once setup() has run, do nothing
}

void wipe()
{

  //Iterate over all bytes in the Arduino's EEPROM
  for(int i = 0; i < EEPROM.length(); i++)
  {
    //Overwrite each byte in EEPROM with 255(empty)
    EEPROM.write(i, 255);
  }
  
}
