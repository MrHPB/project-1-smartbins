//This program is used to wipe the Arduino Uno so that the ESP8266 Wi-Fi Module can be debugged and set to a default baudrate of 9600 using the command 'AT+UART_DEF=9600,8,1,0,0'

//---------- IMPORTANT NOTE ----------
//To debug or change the default baudrate of the ESP8266, the normal wiring scheme can be used with the only changes (with cables still going through voltage dividers) being:
//Arduino TX -> ESP8266 TX
//Arduino RX -> ESP8266 RX
//---------- IMPORTANT NOTE ----------

void setup() 
{
  //Do nothing so that the ESP8266 can be interacted with
}

void loop() 
{
  //Do nothing so that the ESP8266 can be interacted with
}
