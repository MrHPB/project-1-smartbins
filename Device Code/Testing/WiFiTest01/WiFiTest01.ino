#include <SoftwareSerial.h>

#define ARDUINO_BAUD_RATE 9600
#define ESP8266_BAUD_RATE 115200

#define RX_PIN 2
#define TX_PIN 3

#define WIFI_NETWORK ""
#define WIFI_PASSWORD ""

SoftwareSerial esp8266(RX_PIN, TX_PIN);

void setup() 
{

  Serial.begin(ARDUINO_BAUD_RATE);
  
  esp8266.begin(ARDUINO_BAUD_RATE);

  delay(1000);

  Serial.println("--------------------");
  Serial.println("Ready for user input");
  Serial.println("--------------------");
  
}

void loop() 
{
  
  if(esp8266.available())
  {

    Serial.write(esp8266.read());
    
  }

  if(Serial.available())
  {

    esp8266.write(Serial.read());

  }
  
}
