//The required includes for the program
#include <SoftwareSerial.h>
#include <EEPROM.h>

//Define the baud rate required for software serial communications between the Arduino and ESP8266
#define SOFTWARE_SERIAL_BAUD_RATE 9600

//Define the pins for the ESP8266 Wi-Fi Module
#define RX_PIN 3
#define TX_PIN 2

//Define the pins for the HC-SR04 Ultrasonic Sensor
#define TRIG_PIN 9
#define ECHO_PIN 10

//The name of the network the device will connect to and the password required to connect to the network
String network = "bannister01";
String password = "tard1s195";

//The ip address and port number that will be used to connect to the Smart Bins server
String server = "10.0.0.54";
String port = "8000";

//The ID for the device
int deviceID;

//Allow the Arduino and ESP8266 to have serial communications over software serial
SoftwareSerial esp8266(TX_PIN, RX_PIN);

//This function is called automatically each time the device is turned on
void setup()
{ 

  //Set the trig pin to be an output pin and the echo pin to be an input pin
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);

  //Start both the ESP8266 and Arduino at a baud rate of 9600
  Serial.begin(SOFTWARE_SERIAL_BAUD_RATE);
  esp8266.begin(SOFTWARE_SERIAL_BAUD_RATE);

  //Wait for the Arduino to initialize
  while(!Serial)
  {
    //Do nothing while waiting for the Arduino to initialize
  }

  //This variable will store HTTP responses returned from the server
  String httpResponse;

  //Disconnect from any previously connected to networks
  executeWiFiCommand("AT+CWQAP", 1500, "OK");

  //Set the ESP8266 to Station Mode
  executeWiFiCommand("AT+CWMODE_DEF=1", 1000, "OK");

  //Connect to the specified wi-fi network
  executeWiFiCommand("AT+CWJAP_DEF=\"" + network + "\",\"" + password + "\"", 10000, "OK");

  //If the first byte in the Arduino's EEPROM memory is empty (255), then no ID has been set for this device
  if(EEPROM.read(0) == 255)
  {
    
    //Request the lowest available device ID from the server and create a new bin
    httpResponse = sendHttpRequest("", "/app/create_new_bin/");

    //Retrieve the device ID returned from the server
    deviceID = readHttpResponse(httpResponse, "bin_id=", "&end_of_data=true").toInt();

    //Write the lowest available device ID to the Arduino's EEPROM memory so that it is permanently stored in the Arduino
    EEPROM.write(0, deviceID); 
  
  }
  
  //Else if the first byte in the Arduino's EEPROM memory is not empty, then an ID has been set for this device
  else
  {

    //Read the device ID from the first byte in the Arduino's EEPROM memory
    deviceID = EEPROM.read(0);
    
  }
  
}

void loop() 
{
  
  //NOTE - This delay is for demonstration purposes only
  delay(5000);

  //This string will be used for storing device data that will need to be sent to the server
  String data;

  //This variable will store the percentage of the battery remaining for the device
  int batteryRemaining;

  //NOTE - In this version of the Smart Bins project, we were unable to get battery calculation finished, therefore fake battery data is being used. Battery calculation would be a good thing to add in a later version
  batteryRemaining = 100;
  
  //These variables will be used by the Ultrasonic Sensor in order to get the distance of an object from the sensor
  long duration;
  int distance;
  
  //Clear the trig pin
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  
  //Set the trig pin on HIGH state for 10 microseconds
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  //Read the echo pin and return the sound wave travel time in microseconds
  duration = pulseIn(ECHO_PIN, HIGH);
  
  //Calculate the distance of objects from the Ultrasonic sensor
  distance= duration*0.034/2;

  //Print out the data collected by the device
  Serial.print("Device ID: ");
  Serial.println(deviceID);
  Serial.print("Distance: ");
  Serial.println(distance);
  Serial.print("Battery Remaining: ");
  Serial.print(batteryRemaining);
  Serial.println("%");
  Serial.println("");

  //Write all the gathered data to the device data variable in a HTTP POST friendly manner
  data = "bin_id=" + String(deviceID) + "&distance=" + String(distance) + "&battery=" + String(batteryRemaining);

  //Send the gathered data to the server so it can be inserted into the database
  sendHttpRequest(data, "/app/insert_device_data/");
  
}

//This function is used to execute ESP8266 AT commands
String executeWiFiCommand(String command, unsigned long executionTime, String target)
{

  //This variable will hold the response returned by the ESP8266 when a command has finished executing
  String commandResponse = "";

  //This variable will hold the current character returned by the ESP8266 when it is executing a command
  char currentCharacter;

  //This variable is used to indicate if a command has successfully executed
  boolean commandSuccessful = false;

  //These variables indicate the current number of attempts at executing a specified command and the maximum attempts allowed
  int numAttempts = 0;
  int maxAttempts = 6;

  //This variable is used to track how long a command is going for so it can be stopped if it goes for too long
  unsigned long commandTimeout;

  //Execute the ESP8266 AT command passed to this function
  esp8266.println(command);

  //While the command has not finished successfully and the maximum number of attempts allowed have not been exceeded
  while(commandSuccessful == false && numAttempts < maxAttempts)
  {

    //Determine the maximum amount of time allowed for the command
    commandTimeout = (millis() + executionTime) + (numAttempts * 1000);

    //While the command has not exceeded its maximum allowed time
    while(millis() < commandTimeout)
    {

      //While there are still characters to retrieve from the ESP8266
      while(esp8266.available())
      {

        //Get the current character available in the ESP8266 and add it to the command response variable
        currentCharacter = esp8266.read();
        commandResponse.concat(currentCharacter);
    
      } 
  
    }

    //Print the output of the command
    Serial.println(commandResponse);
    
    //If the command's response contains the desired string
    if(commandResponse.indexOf(target) != -1)
    {

      //Set commandSuccessul to true to stop the command from being executed again
      commandSuccessful = true;
    
    }

    //If the command's response did not contain the desired string, increase numAttempts by 1
    numAttempts++;

  }

  //If the command could not be successfully executed
  if(commandSuccessful == false)
  {

    //Put the command into an infinite loop so that it will not continue to execute other commands
    Serial.println("SLEEPING");
    while(true)
    {
      
    }
    
  }


  //Return the full command response
  return(commandResponse);

}

//This function is used to send a HTTP POST request to the server
String sendHttpRequest(String requestData, String url)
{

  //This variable is used for storing the HTTP response returned by the server
  String httpResponse; 

  //Create the HTTP POST request that will be sent to the server
  String request = 
    "POST " + url + " HTTP/1.1\r\n" +
    "Host: " + server + "\r\n" +
    "Accept: *" + "/" + "*\r\n" +
    "Content-Length: " + requestData.length() + "\r\n" +
    "Content-Type: application/x-www-form-urlencoded\r\n" +
    "\r\n" + 
    requestData;

  //Establish a TCP connection to the server
  executeWiFiCommand("AT+CIPSTART=\"TCP\",\"" + server + "\"," + port, 5000, "OK");

  //Establish a SSL connection to the server
  //executeWiFiCommand("AT+CIPSTART=\"SSL\",\"" + server + "\"," + port, 5000, "OK");

  //Get the length of the HTTP POST request and establish a buffer for it
  executeWiFiCommand("AT+CIPSEND=" + String(request.length()), 1000, ">");

  //Send the HTTP POST request to the server and store the response
  httpResponse = executeWiFiCommand(request, 5000, "end_of_data=true");

  //Close the TCP connection to the server
  executeWiFiCommand("AT+CIPCLOSE", 500, "OK");

  //Return the HTTP response returned by the server
  return httpResponse;
  
}

//This function is used to retrieve data from HTTP responses returned by the server by getting a substring positioned between a start and end substring
String readHttpResponse(String response, String startPoint, String endPoint)
{

  //Get the index of both the start and end substrings
  int startPointIndex = response.indexOf(startPoint);
  int endPointIndex = response.indexOf(endPoint);

  //If the start and end substrings can be found in the HTTP response and the index of the starting substring is less then the index of the ending substring
  if((startPointIndex != -1 && endPointIndex != -1) && (startPointIndex < endPointIndex))
  {

    //Return the substring inbetween the start and end substrings
    return(response.substring(startPointIndex + (startPoint.length()), endPointIndex));
      
  }
  
  //If the start and end substrings are not found or if the end substring index is greater than the start substring index
  else
  {
    
    //Return an error message
    return("ERROR");
      
  }
  
}
