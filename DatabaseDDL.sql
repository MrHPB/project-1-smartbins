DROP DATABASE IF EXISTS SMARTBINS;

CREATE DATABASE SMARTBINS CHARACTER SET utf8 COLLATE utf8_bin;

USE SMARTBINS;

GRANT ALL PRIVILEGES 
  ON SMARTBINS.*
  TO 'root'@'localhost'
  IDENTIFIED BY 'password'; 

-- CREATE TABLE Users
-- (
-- 	UserID INT AUTO_INCREMENT NOT NULL,
-- 	FirstName VARCHAR(50) NOT NULL,
-- 	LastName VARCHAR(50) NOT NULL,
-- 	Email VARCHAR(50) NOT NULL,
-- 	Password VARCHAR(255) NULL,
-- 	IsAdmin BOOL DEFAULT 0,
-- 	PRIMARY KEY (UserID)
-- );


-- CREATE TABLE BinTypes
-- (
-- 	TypeID INT AUTO_INCREMENT NOT NULL,
-- 	TypeDescription VARCHAR(50) NOT NULL,
-- 	TotalVolume INT NOT NULL,
-- 	Height INT NOT NULL,
-- 	PRIMARY KEY (TypeID)
-- );

-- CREATE TABLE Campuses
-- (
-- 	CampusID INT AUTO_INCREMENT NOT NULL,
-- 	Name VARCHAR(50) NOT NULL,
-- 	PRIMARY KEY (CampusID)
-- );

-- CREATE TABLE CampusLocations
-- (
-- 	LocationID INT AUTO_INCREMENT NOT NULL,
-- 	CampusID INT NOT NULL,
-- 	Description VARCHAR(50) NOT NULL,
-- 	PRIMARY KEY (LocationID),
-- 	FOREIGN KEY (CampusID) REFERENCES Campuses(CampusID)
-- );

-- CREATE TABLE Bins
-- (
-- 	BinID INT AUTO_INCREMENT NOT NULL,
-- 	CampusID INT NOT NULL,
-- 	CampusLocationID INT NOT NULL,
-- 	TypeID INT NOT NULL,
-- 	PRIMARY KEY (BinID),
-- 	FOREIGN KEY (CampusID) REFERENCES Campuses(CampusID),
-- 	FOREIGN KEY (CampusLocationID) REFERENCES CampusLocations(LocationID),
-- 	FOREIGN KEY (TypeID) REFERENCES BinTypes(TypeID)
-- );


-- CREATE TABLE DeviceLogs
-- (
-- 	LogTimestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
-- 	BinID INT NOT NULL,
-- 	Distance INT NOT NULL,
-- 	BatteryRemaining INT NOT NULL,
-- 	PRIMARY KEY (LogTimestamp),
-- 	FOREIGN KEY (BinID) REFERENCES Bins(BinID)
-- );

-- INSERT INTO Campuses VALUES (0, "TestCampus");
-- INSERT INTO CampusLocations VALUES (0, 0, "This is a test campus location");
-- INSERT INTO BinTypes VALUES (0, "TestBinType", 50, 300);

-- INSERT INTO Bins VALUES (0, 0, 0, 0);

-- INSERT INTO DeviceLogs (BinID, Distance, BatteryRemaining) VALUES (0, 5, 70);